const axios = require("axios");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
let _ = require("lodash");

/**
 * Mecanisme pour récupéré les données sur LDLC
 */

module.exports = ldlcHandler = (url, runCompleteProcedure = false) => {
    if (runCompleteProcedure) {
        return completeProcedure(url);
    } else {
        return PriceProcedure(url);
    }
};

const completeProcedure = (url) => {
    let result = axios.get(url).then(async ({ data: html }) => {
        const dom = await new JSDOM(html);
        let title = dom.window.document.querySelector("h1").innerHTML.trim();
        let price = dom.window.document
            .querySelector("#product-page-price")
            .getAttribute("data-price");
        let photoLink = dom.window.document
            .querySelector(".swiper-slide")
            .querySelector("a")
            .getAttribute("href");

        /**
         * Debut procedure recuperation énergetique
         * si elle existe l'action ce fera , sinon elle retournera NC dans les data
         */
        let classeEnergetique = "NC";
        try {
            let resultsecond = dom.window.document
                .querySelector(".details-pdt-desc section:last-child ul")
                .querySelectorAll("li");

            let arrayToFetch = [];
            resultsecond.forEach((e) => {
                arrayToFetch.push(e.textContent);
            });

            arrayToFetch.forEach((e) => {
                if (e.includes("Classe énergétique")) {
                    let result = e.split(" ");
                    classeEnergetique = result[result.length - 1];
                }
            });
        } catch (error) {
            classeEnergetique = "NC";
        }
        /**
         * Fin procedure recuperation énergetique
         */

        let dataObject = {
            title: title,
            urlImage: photoLink,
            price: price,
            classeEnergetique: classeEnergetique,
        };
        return dataObject;
    });
    return result;
};

const PriceProcedure = (url) => {
    let result = axios.get(url).then(async ({ data: html }) => {
        const dom = await new JSDOM(html);
        let price;
        try {
            price = dom.window.document
                .querySelector("#product-page-price")
                .getAttribute("data-price");
        } catch (error) {
            price = 0;
        }

        let dataObject = {
            price: price,
        };
        return dataObject;
    });
    return result;
};
