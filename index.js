const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const autoUpdatePrice = require("./routes/autoUpdatePrice");
require("dotenv").config();
const cors = require("cors");

app.use(cors());

// create application/json parser
let jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
let urlencodedParser = bodyParser.urlencoded({ extended: false });

/**
 * RAPPEL :
 * Etude pour creation d'un autorun avec horaire (automatique) pour effectué la mise a jour de la bdd suivant les url
 * qui ont été ajouté dans notre bdd
 * exemple : l'autorun s'executera tout les jours a 18h , il effectura une requete vers la bdd pour récupéré la liste des url
 * puis tchequera tout les site , mettra a jour les prix puis les push dans la bdd
 */

/**
 * TODO :
 *  - Faire l'enregistrement en BDD (etudié l'architecture de la BDD)
 *  - Crée route pour enregistré lien en BDD , si le lien n'existe pas , faire l'update de donnée pour cette articles
 *  - Récupéré les données sur un site (LDLC) puis les enregistré en BDD
 *  - Crée l'autorun
 */

/**
 * Route :
 * GET / : retourne les infos sur l'api .
 * POST /product/add : Permet de posté une url (seulement ldlc actuellement) (body : url => url)
 * GET /product/infos/:id : permet de récupéré les information produit + sont historique
 */

app.use("/", jsonParser, require("./routes/home"));
app.use("/product", urlencodedParser, require("./routes/addUrlToBDD"));
app.use("/product/infos", urlencodedParser, require("./routes/getInfoProduct"));
app.use("/product/list", urlencodedParser, require("./routes/getProductsList"));
app.use("/product/limit", urlencodedParser, require("./routes/getProductListLimited"));
app.use("/search/", urlencodedParser, require("./routes/querySearch"));
app.use("/product/popularity", urlencodedParser, require("./routes/getProductPopularity"));

app.listen(8000);
autoUpdatePrice();
