const express = require("express");
const router = express.Router();
const moment = require("moment");
moment.locale("fr");

/**
 * Permet de vérifié le bon fonctionnement de l'api , infos retournée (a voir)
 * temps de fonctionnement , nombre de lien dans la bdd , date dernieres update
 */

router.get("/", async (req, res) => {
    try {
        let [nbrLink] = await pool.promise().query("SELECT 'url' FROM articles");
        let [lastUpdateQuery] = await pool
            .promise()
            .query("SELECT * FROM last_update_date ORDER BY id DESC LIMIT 1");
        let [nbrHistoryData] = await pool
            .promise()
            .query("SELECT COUNT(id) as quantity FROM historiques");

        let lastUpdate = moment(lastUpdateQuery[0].date).format("DD-MM-YYYY LTS");
        let information = {
            quantityUrlInBDD: nbrLink.length,
            lastUpdate: lastUpdate,
            nbrHistoryData: nbrHistoryData[0].quantity,
        };
        res.status(200).json(information);
    } catch (error) {
        res.status(500).send("Serveur HS");
    }
});

module.exports = router;
