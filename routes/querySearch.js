const express = require("express");
const moment = require("moment");
moment.locale("fr");
const router = express.Router();
require("../mysql/connect");

/**
 * Permet d'effectué une recherche par nom d'article
 * ceci effectue un retour des articles trouver avec ce nom
 */

router.get("/:query", async (req, res) => {
    try {
        let [articles] = await pool
            .promise()
            .query(`SELECT * FROM articles WHERE nom LIKE '%${req.params.query}%'`);

        for (let i = 0; i < articles.length; i++) {
            // console.log(articles[i].id);
            let [historiques] = await pool
                .promise()
                .query(
                    `SELECT * FROM historiques WHERE id_article = ${articles[i].id} ORDER BY 'date_ajout' DESC LIMIT 1`
                );
            articles[i]["dernier_prix"] = historiques[0].prix;
        }

        res.status(200).json(articles);
    } catch (error) {
        res.status(500).json({ msg: "api non disponible" });
    }
});

module.exports = router;
