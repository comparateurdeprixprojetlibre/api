const express = require("express");
const moment = require("moment");
moment.locale("fr");
const router = express.Router();
require("../mysql/connect");

/**
 * permet de récupéré les donnée d'un articles , ainsi que sont historique sous le format JSON
 */

router.get("/:id", async (req, res) => {
    try {
        let id = parseInt(req.params.id);
        if (Number.isInteger(id) && Math.sign(id)) {
            let combineData = { data: [], historiques: [] };
            let [data] = await pool.promise().query("SELECT * FROM articles WHERE `id` = ?", [id]);
            await pool
                .promise()
                .query(`UPDATE articles SET nbr_de_vue = nbr_de_vue +1 WHERE id = ?`, [id]);

            let [historiques] = await pool
                .promise()
                .query("SELECT * FROM historiques WHERE `id_article` = ?", [id]);

            for (let i = 0; i < data.length; i++) {
                const element = data[i];
                combineData.data.push(element);
            }
            for (let i = 0; i < historiques.length; i++) {
                const element = historiques[i];
                combineData.historiques.push(element);
            }

            res.status(200).json(combineData);
        }
    } catch (error) {
        res.status(500).json({ msg: "api non disponible" });
    }
});

module.exports = router;
