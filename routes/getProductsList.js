const express = require("express");
const moment = require("moment");
moment.locale("fr");
const router = express.Router();
require("../mysql/connect");

/**
 * Permet de récupéré la liste de tout les articles en BDD avec leur information +
 *  leur derniers prix ajouté
 */

router.get("/", async (req, res) => {
    try {
        let dataFetch = [];
        let dataUpdate = [];

        let [data] = await pool.promise().query("SELECT * FROM articles");

        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            dataFetch.push(element);
            let id = data[i].id;
            let [historiques] = await pool
                .promise()
                .query(
                    "SELECT * FROM historiques WHERE `id_article` = ? ORDER BY id DESC LIMIT 1",
                    [id]
                );

            for (let z = 0; z < historiques.length; z++) {
                const element = historiques[z];
                let newObjUpdate = { ...dataFetch[i], lastPrice: element.prix };
                dataUpdate.push(newObjUpdate);
            }
        }

        res.status(200).json(dataUpdate);
    } catch (error) {
        res.status(500).json({ msg: "api non disponible" });
    }
});

module.exports = router;
