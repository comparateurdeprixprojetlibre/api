const express = require("express");
const router = express.Router();
const ldlcHandler = require("../RequestWebSiteAction/LDLC/ldlcHandler");
const moment = require("moment");
// const antiSleepHosting = require("../utils/antiSleepHosting");
moment.locale("fr");
const { PerformanceObserver, performance } = require("perf_hooks");

/**
 * Permet de récupéré les data (ligne 28 a 1 recuperation de toute les donner , sinon 0 recuperation que des prix)
 */

module.exports = autoUpdatePrice = async () => {
    try {
        let t0 = performance.now();
        let timeToStart = moment().format("DD-MM-YYYY LTS");
        console.log("La mise a jour a commencé a " + timeToStart);

        /**
         * Recupération de toute les urls
         */
        let [urlList] = await pool.promise().query("SELECT `id` ,`url` FROM articles");
        let data = [];

        /**
         * Récuperation des information , puis push des données dans la BDD
         */
        for (let i = 0; i < urlList.length; i++) {
            let result = await ldlcHandler(urlList[i].url, 0);
            let date = moment().format("YYYY-MM-DD LTS");
            data.push(result);

            await pool
                .promise()
                .query(
                    "INSERT INTO `historiques` (`id`, `id_article`, `prix`, `date_ajout`) VALUES (?, ?, ?, ?);",
                    [null, urlList[i].id, parseFloat(result.price), date]
                );
        }
        let date = moment().format("DD-MM-YYYY LTS");
        console.log("Mise a jour terminé à " + date);
        date = moment().format("YYYY-MM-DD LTS");
        /**
         * Ajout de la date de la dernieres update a la BDD
         */
        await pool
            .promise()
            .query("INSERT INTO `last_update_date` (`id`, `date`) VALUES (?, ?);", [null, date]);

        setTimeout(() => {
            autoUpdatePrice();
            // antiSleepHosting();
        }, 600000);
        let t1 = performance.now();
        console.log(
            "la fonction a été executé en " +
                (t1 - t0) / 1000 +
                " Pour " +
                urlList.length +
                " URL ,  soit " +
                (t1 - t0) / 1000 / urlList.length
        );
    } catch (error) {
        console.log(error);
        return false;
    }
};
