const express = require("express");
const moment = require("moment");
moment.locale("fr");
const router = express.Router();
const ldlcHandler = require("../RequestWebSiteAction/LDLC/ldlcHandler");
require("../mysql/connect");
const checkUrlList = require("../utils/validUrl");

/**
 * Permet l'ajout d'un produit qui n'existe pas encore , + ajout la premiere entré dans la table historique
 * Si existe retourne l'id de l'article pour permet la redirection de l'utilisateur sur cette page
 */

router.post("/add", async (req, res) => {
    try {
        let urlToAdd = req.body.url;
        if (urlToAdd.length > 0 && checkUrlList(urlToAdd)) {
            let [data] = await pool
                .promise()
                .query("SELECT * FROM articles WHERE `url` = ?", [urlToAdd]);

            if (data.length > 0) {
                res.status(200).json({ msg: "url existante", id: data[0].id });
            } else {
                let result = await ldlcHandler(urlToAdd, 1);
                let date = moment().format("YYYY-MM-DD LTS");
                console.log(date);

                /**
                 * Ajout de l'articles
                 */

                let [id] = await pool
                    .promise()
                    .query(
                        "INSERT INTO `articles` (`id`, `nom`, `image`, `url`, `norme_eco`, `date_ajout`) VALUES (?, ?, ?, ?, ?, ?);",
                        [
                            null,
                            result.title,
                            result.urlImage,
                            urlToAdd,
                            result.classeEnergetique,
                            date,
                        ]
                    );
                id = id.insertId;

                /**
                 * Vue que c'est la premiere entré de cette articles , ajout de sont premier prix.
                 */

                await pool
                    .promise()
                    .query(
                        "INSERT INTO `historiques` (`id`, `id_article`, `prix`, `date_ajout`) VALUES (?, ?, ?, ?);",
                        [null, id, parseFloat(result.price), date]
                    );

                res.status(200).json({ msg: "Url ajouté avec succée" });
            }
        } else {
            res.status(400).json({ msg: "url invalide" });
        }
    } catch (error) {
        res.status(500).send("Serveur HS");
    }
});

module.exports = router;
