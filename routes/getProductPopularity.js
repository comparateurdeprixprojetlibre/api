const express = require("express");
const moment = require("moment");
moment.locale("fr");
const router = express.Router();
require("../mysql/connect");

/**
 * permet de récupéré Les 3 Produit les plus populaire
 */

router.get("/", async (req, res) => {
    try {
        // let combineData = [];
        let [data] = await pool
            .promise()
            .query("SELECT * FROM articles ORDER BY 'nbr_de_vue' DESC LIMIT 3");

        for (let i = 0; i < data.length; i++) {
            let [historiques] = await pool
                .promise()
                .query(
                    "SELECT * FROM historiques WHERE `id_article` = ? ORDER BY 'date_ajout' DESC",
                    [data[i].id]
                );
            data[i]["dernier_prix"] = historiques[0].prix;
        }
        res.status(200).json(data);
    } catch (error) {
        res.status(500).json({ msg: "api non disponible" });
    }
});

module.exports = router;
